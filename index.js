var server = require('./app/core/server'),
	path = require('path'),
	config = require(path.resolve('./app/config/config')),
    uncaughtProcessExceptionHandler = require('./app/core/uncaughtProcessExceptionHandler');

//process.on('uncaughtException', uncaughtProcessExceptionHandler);

server.listen(config.port, function() {
  console.log('%s listening at %s\n', server.name, server.url);
});