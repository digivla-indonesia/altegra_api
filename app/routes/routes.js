module.exports = function(server){
	var path = require('path');
	var config = require(path.resolve('./app/config/config'));
	var auth = require(path.resolve('./app/auths/auth'));

	server.get('/', function(req, res, next){
		res.status(200);
		return res.json(config.responseMessage(200, true, 'Welcome to ' + config.applicationName + ' REST API Services', ''));
	});
	
	/* v1.0.0 */
	var formedv100 = require(path.resolve('./app/controllers/v1.0.0/formed.controller.js'));
	var socmedv100 = require(path.resolve('./app/controllers/v1.0.0/socmed.controller.js'));


	server.post({path: '/client/login', version: '1.0.0'}, auth.login);

	server.post({path: '/client/getassets', version: '1.0.0'}, formedv100.getGroupCategory);
	server.post({path: '/formed/graph', version: '1.0.0'}, formedv100.graph);
	server.post({path: '/formed/graph3', version: '1.0.0'}, formedv100.graph3);
	server.post({path: '/formed/graph4', version: '1.0.0'}, formedv100.graph4);
	server.post({path: '/formed/graph6', version: '1.0.0'}, formedv100.graph6);
	server.post({path: '/formed/getews', version: '1.0.0'}, formedv100.oldEWS);
	server.post({path: '/formed/getnewslist', version: '1.0.0'}, formedv100.getNewsList);

	server.post({path: '/socmed/wordcloud', version: '1.0.0'}, socmedv100.getWordCloud);
}
