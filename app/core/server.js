var restify = require('restify'),
    log = require('./logger'),
    config = require('../config/config'),
    jwt    = require('jsonwebtoken'),
    versioning = require('restify-url-semver'),
    uncaughtServerExceptionHandler = require('./uncaughtServerExceptionHandler');

var server = restify.createServer({
  name: config.applicationName,
  log: log
});

server.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8888');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

server.use(restify.acceptParser(server.acceptable));
server.pre(versioning({ prefix: '/api' }));
server.use(restify.authorizationParser());
server.use(restify.dateParser());
server.use(restify.queryParser());
server.use(restify.jsonp());
server.use(restify.gzipResponse());
server.use(restify.bodyParser());
server.on('uncaughtException', uncaughtServerExceptionHandler);

server.use(function(req, res, next){
  var path = req.path();
  var auth;

  if (req.headers.authorization) {
    auth = new Buffer(req.headers.authorization.substring(6), 'base64').toString().split(':');
  }

  if (!auth || auth[0] !== 'altegralabs' || auth[1] !== config.password) {
    res.status(401);
    return res.json(config.responseMessage(401, false, 'Not authorized', 'You are not authorized!'));
  }else {
    next();
  }

  // No authentication needed
  if(path === '//client/login' || path === '/register') return next();

  // var tokens = decodeURIComponent(req.headers['x-access-token']);
  // if(tokens){
  //   jwt.verify(tokens, config.jwtsecret, function(err, decoded){
  //     if(err){
  //       res.status(200);
  //       return res.json(config.responseMessage(401, false, err.name, 'You have to login first'));
  //     }
  //     next();
  //   });
  // } else{
  //   return res.json(config.responseMessage(401, false, 'There is no token!!', ''));
  // }

  next();
});

server.use(function(req,res,next){
  res.setHeader('content-type','application/json');
  //   OR
  // res.setHeaders({'content-type'}:'application/json'});

  next();
})

server.pre(function(req, res, next) {
  req.log.info({
    time: new Date().getTime(),
    id: req.id(),
    method: req.method,
    path: req.path(),
    state: "started"
  });
  next();
});

server.on('after', function(req, res, route) {
  req.log.info({
    time: new Date().getTime(),
    id: req.id(),
    // method: route.spec.method,
    // path: route.spec.path,
    statusCode: res.statusCode,
    length: res.headers()['content-length'],
    state: "finished"
  });
});

var routes = require('../routes/routes')(server);

module.exports = server;