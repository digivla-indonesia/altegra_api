var elasticSearch   = require('elasticsearch');
var path			      = require('path');
var config          = require(path.resolve('./app/config/config'));
var request			    = require('request');
var elasticService  = require(path.resolve('./app/models/elastic.model.js'));
var nodeService     = require(path.resolve('./app/models/api-node.model.js'));
var jsonTemplates   = require(path.resolve('./app/models/template-json.model.js'));
var restService 	  = require(path.resolve('./app/models/dashboard.server.api.model'));
var elasticService 	= require(path.resolve('./app/models/elastic.model.js'));
var jwt             = require('jsonwebtoken');
var async           = require('async');
var is_json 	      = require('is-json');

var client = new elasticSearch.Client({
	host: "http://elastic.antara-insight.id",
  	log: 'trace'
});



function formedController(){

  this.getGroupCategory = function(req, res, next){
    var token           = jwt.decode(req.headers['x-access-token']) || '';
    var client_id       = token.client_id;


    getToken(function(coreToken){
      var reqJson = {
        client_id: client_id,
        token: coreToken
      }, uriSegment = {
        first: 'tb_category_set_type',
        second: ''
      };

      nodeService(uriSegment, reqJson, function(resp) {
        if (!resp.success) {
          // return cb({
          //   success: false,
          //   data: []
          // });
          res.status(304);
          return res.json(config.responseMessage(304, false, 'Cannot getting client group category', {}));
        }

        res.status(200);
        return res.json(config.responseMessage(200, true, 'Getting client group category', resp.data));
      });  
    });
    
  };

  this.graph = function(req, res, next){ 
    var token           = jwt.decode(req.headers['x-access-token']) || '';
    var client_id       = token.client_id;
    var group_category  = (typeof req.body.gc != 'undefined') ? req.body.gc : ''; 
    var media_set       = (typeof req.body.ms != 'undefined') ? req.body.ms : ''; 
    var category_id     = (typeof req.body.sc != 'undefined') ? req.body.sc : ''; 
    var media_id        = (typeof req.body.mi != 'undefined') ? req.body.mi.toString() : '';  
    var time_frame      = (typeof req.body.tf != 'undefined') ? parseInt(req.body.tf) : ''; 
    var date_from       = (typeof req.body.df != 'undefined') ? req.body.df : ''; 
    var date_to         = (typeof req.body.dt != 'undefined') ? req.body.dt : '';
    var uriSegment = {
      index: 'client_' + client_id,
      type: 'datalist',
      method: 'POST'
    };

    if(time_frame == 0 && (date_from == '' || date_to == '' || client_id === '')){
      return res.json(config.responseMessage(400, false, 'Wrong parameter', {}));
    }

    async.waterfall([
      function(cb){
        getToken(function(digivlaToken){
          cb(null, digivlaToken);
        });
      },
      function(digivlaToken, cb){
        getMediaId(client_id, media_set, digivlaToken, function(medArr){
          getCategorySet(client_id, group_category, true, digivlaToken, function(catArr){
            if(media_id !== '00'){
              medArr = {
                success: true,
                data: [media_id]
              };
            }

            if(category_id !== 'All Category'){
              catArr = {
                success: true,
                data: {
                  processed: [{
                    match: {
                      category_id: {
                        query: category_id,
                        operator: 'and'
                      }
                    }
                  }],
                  raw: [category_id]
                }
              };
            }

            cb(null, digivlaToken, medArr, catArr);
          });
        });
      },
      function(digivlaToken, medArr, catArr, cb){
        var dateProcessing = dateFormatting(time_frame, date_from, date_to);

        var items = {
          category: (catArr.data.processed.length > 0) ? catArr.data.processed : [],
          media: (medArr.data.length > 0) ? medArr.data : [],
          date_from: dateProcessing.date_from,
            date_to: dateProcessing.date_to,
            interval: dateProcessing.interval
        };

        var params = jsonTemplates.getProcess(items);
        cb(null, digivlaToken, catArr, params);
      },
      function(digivlaToken, catArr, params, cb){
        elasticService(uriSegment, params, function(resp){
          var arrayForChart = [];
          var result = resp.data.aggregations.category_id.buckets;

          for(var i=0;i<result.length;i++){
            if(result[i].doc_count > 0 && catArr.data.raw.indexOf(result[i].key) > -1){
              var buckets = result[i];
              var bucketsArray = [buckets.key, buckets.doc_count];

              arrayForChart.push(bucketsArray);
            }
          }

          cb(null, arrayForChart);
        });
      }
    ], function(err, result){
      if(err){
       console.log(err);
       res.status(302);
       return res.json(config.responseMessage(302, false, 'Data not found', {}));
      }

      res.status(200);
      return res.json(config.responseMessage(200, true, 'Getting result from graph', result));
    });
  };

  this.graph3 = function(req, res, next){
    var token           = jwt.decode(req.headers['x-access-token']) || '';
    var client_id       = token.client_id;
    var group_category  = (typeof req.body.gc != 'undefined') ? req.body.gc : ''; 
    var media_set       = (typeof req.body.ms != 'undefined') ? req.body.ms : ''; 
    var category_id     = (typeof req.body.sc != 'undefined') ? req.body.sc : ''; 
    var media_id        = (typeof req.body.mi != 'undefined') ? req.body.mi.toString() : '';  
    var time_frame      = (typeof req.body.tf != 'undefined') ? parseInt(req.body.tf) : ''; 
    var date_from       = (typeof req.body.df != 'undefined') ? req.body.df : ''; 
    var date_to         = (typeof req.body.dt != 'undefined') ? req.body.dt : '';
    var mediaType       = (typeof req.body.media_type != 'undefined') ? req.body.media_type : 'print';
    var code            = req.body.code;
    var currentDate     = new Date();
    var dateFlag        = new Date();
    var interval        = 'day';
    var uriSegment = {
      index: 'client_' + client_id,
      type: 'datalist',
      method: 'POST'
    };

    if(time_frame == 0 && (date_from == '' || date_to == '' || client_id === '')){
      return res.json(config.responseMessage(400, false, 'Wrong parameter', {}));
    }

    async.waterfall([
      function(cb){
        getToken(function(digivlaToken){
          cb(null, digivlaToken);
        });
      },
      function(digivlaToken, cb){
        getMediaByType(mediaType, function(medias){
          cb(null, digivlaToken, medias);
        });
      },
      function(digivlaToken, medias, cb){
        getCategorySet(client_id, group_category, false, digivlaToken, function(catArr){

            if(category_id !== 'All Category'){
                catArr = {
                  success: true,
                  data: {
                    processed: [{
                          match: {
                          category_id: {
                              query: category_id,
                              operator: 'and'
                            }
                        }
                      }],
                      raw: [category_id]
                  }
                };
            }

            cb(null, digivlaToken, medias, catArr);
        });
      },
      function(digivlaToken, medias, catArr, cb){
        var dateProcessing = dateFormatting(time_frame, date_from, date_to);

        var items = {
          category: (catArr.data.length > 0) ? catArr.data : [],
          media: (medias.length > 0) ? medias : [],
          date_from: dateProcessing.date_from,
          date_to: dateProcessing.date_to,
          interval: dateProcessing.interval,
          code: code
        };

        var params = jsonTemplates.getProcess3(items);

        cb(null, digivlaToken, params);
      },
      function(digivlaToken, params, cb){
        elasticService(uriSegment, params, function(resp){
          var arrayForCategory = [];
          var arrayForSeries = [
            {
              name: 'Positive',
              data: []
            }, {
              name: 'Neutral',
              data: []
            }, {
              name: 'Negative',
              data: []
            }
          ];
            

          if(resp.data.hasOwnProperty('aggregations')){

            var result = resp.data.aggregations.datagroup.buckets;
            // console.log(JSON.stringify(result));

            if(code === '050'){
              var numberToDisplay = 10, numberFlag = 0;
              var disposableValue, disposableBuckets, disposableTotal;
              var dataArray = [];
              var contoh = [
                {
                  "labels": ["Positive", "Others"],
                  "data": [70, 30]
                },
                {
                  "labels": ["Netral", "Others"],
                  "data": [20, 80]
                }
              ];

              var categoryDetail = {};

              getMedia(function(medList){
                var getPercentage = function(n, total){
                  return Math.round((n / total) * 100);
                };

                for(var i=0;i<result.length;i++){
                  var buckets = result[i];
                  var bucketsArray = [];
                  var tempJSON = {};

                  disposableBuckets = [];
                  disposableValue = 0;
                  disposableTotal = 0;


                  disposableBuckets = buckets.tone.buckets;


                  if(disposableBuckets.length > 0){
                    if(disposableBuckets.length > 1) disposableValue += disposableBuckets[1].doc_count;
                    if(disposableBuckets.length > 2) disposableValue += disposableBuckets[2].doc_count;

                    disposableTotal = disposableBuckets[0].doc_count + disposableValue;


                    // uncomment this for getting exact value
                    // dataArray.push({
                    //   labels: [ medList.data[buckets.key.toString()], disposableBuckets[0].key ],
                    //   data: [ disposableBuckets[0].doc_count, disposableValue ]
                    // });

                    // uncomment this for getting value in percentage
                    dataArray.push({
                      labels: [ medList.data[buckets.key.toString()], disposableBuckets[0].key ],
                      data: [ getPercentage(disposableBuckets[0].doc_count, disposableTotal), getPercentage(disposableValue, disposableTotal) ]
                    });

                  }
                }

                cb(null, dataArray);
              });

            // coverage tonality
            } else if(code === '018'){ 
              var dataArray = [[], [], []];

              for(var i=0;i<result.length;i++){
                var buckets = result[i]
                var bucketsArray = [];
                var tempJSON = {};
                var dateFormatter = function(tgl){
                  return tgl.getFullYear() + '-' + ('0' + (tgl.getMonth()+1)).slice(-2) + '-' + ('0' + tgl.getDate()).slice(-2);
                }

                arrayForCategory.push(dateFormatter(new Date(buckets.key)));

                for(var j=0;j<buckets.tone.buckets.length;j++){
                  tempJSON[buckets.tone.buckets[j].key.toString()] = buckets.tone.buckets[j].doc_count;
                }

                dataArray[0].push((tempJSON.hasOwnProperty('1')) ? tempJSON['1'] : 0);
                dataArray[1].push((tempJSON.hasOwnProperty('0')) ? tempJSON['0'] : 0);
                dataArray[2].push((tempJSON.hasOwnProperty('-1')) ? tempJSON['-1'] : 0);

              }

              cb(null, {
                labels: arrayForCategory,
                series: ['Positive', 'Neutral', 'Negative'],
                data: dataArray
              });

            } else{
              for(var i=0;i<result.length;i++){
                var buckets = result[i]
                var bucketsArray = [];
                var tempJSON = {};

                arrayForCategory.push(buckets.key);

                for(var j=0;j<buckets.tone.buckets.length;j++){
                  tempJSON[buckets.tone.buckets[j].key.toString()] = buckets.tone.buckets[j].doc_count;
                }

                arrayForSeries[0].data.push((tempJSON.hasOwnProperty('1')) ? tempJSON['1'] : 0);
                arrayForSeries[1].data.push((tempJSON.hasOwnProperty('0')) ? tempJSON['0'] : 0);
                arrayForSeries[2].data.push((tempJSON.hasOwnProperty('-1')) ? tempJSON['-1'] : 0);
              }

              cb(null, {
                category: arrayForCategory,
                series: arrayForSeries
              });
            }

          } else{

            cb('no data', {
                labels: [],
                series: ['Positive', 'Neutral', 'Negative'],
                data: []
              });

          }

        });
      }
    ], function(err, result){
      if(err){
        console.log(err);
        res.status(302);
        return res.json(config.responseMessage(302, false, 'Data not found', {}));
      }

      res.status(200);
      return res.json(config.responseMessage(200, true, 'Getting result from graph3', result));
    });

  };

	this.graph4 = function(req, res, next){	
    var token           = jwt.decode(req.headers['x-access-token']) || '';
	  var client_id 	    = token.client_id;
    var group_category  = (typeof req.body.gc != 'undefined') ? req.body.gc : '';	
    var media_set	      = (typeof req.body.ms != 'undefined') ? req.body.ms : '';	
    var category_id	    = (typeof req.body.sc != 'undefined') ? req.body.sc : '';	
    var media_id	      = (typeof req.body.mi != 'undefined') ? req.body.mi.toString() : '';	
    var time_frame	    = (typeof req.body.tf != 'undefined') ? parseInt(req.body.tf) : '';	
    var date_from	      = (typeof req.body.df != 'undefined') ? req.body.df.substring(0, 10) : '';	
    var date_to	        = (typeof req.body.dt != 'undefined') ? req.body.dt.substring(0, 10) : '';
    var mediaType       = (typeof req.body.media_type != 'undefined') ? req.body.media_type : 'print';
	  var uriSegment = {
		  index: 'client_' + client_id,
	    type: 'datalist',
	    method: 'POST'
	  };

  	if(time_frame == 0 && (date_from == '' || date_to == '' || client_id === '')){
      return res.json(config.responseMessage(400, false, 'Wrong parameter', {}));
  	}

	  async.waterfall([
  		  function(cb){
  			  getToken(function(digivlaToken){
  				  cb(null, digivlaToken);
  			 });
  		  },
        function(digivlaToken, cb){
          getMediaByType(mediaType, function(medias){
            cb(null, digivlaToken, medias);
          });
        },
  	    function(digivlaToken, medias, cb){
          getCategorySet(client_id, group_category, true, digivlaToken, function(catArr){

              if(category_id !== 'All Category'){
                  catArr = {
                    success: true,
                    data: {
                      processed: [{
                            match: {
                            category_id: {
                                query: category_id,
                                operator: 'and'
                              }
                          }
                        }],
                        raw: [category_id]
                    }
                  };
              }

              cb(null, digivlaToken, medias, catArr);
          });
  	    },
  	    function(digivlaToken, medias, catArr, cb){
  	    	var dateProcessing = dateFormatting(time_frame, date_from, date_to);

  	    	var items = {
  		        category: (catArr.data.processed.length > 0) ? catArr.data.processed : [],
              media: (medias.length > 0) ? medias : [],
  		        date_from: dateProcessing.date_from,
  		        date_to: dateProcessing.date_to,
  		        interval: dateProcessing.interval
  		    };

  	    	var params = jsonTemplates.getProcess4(items);

  	    	cb(null, digivlaToken, catArr, params);
  	    },
  	    function(digivlaToken, catArr, params, cb){
  	    	elasticService(uriSegment, params, function(resp){
  	        
  		        // console.log('params: ' + JSON.stringify(params));
  		        // console.log('response: ' + JSON.stringify(resp));
  		        // console.log('catArr: ' + JSON.stringify(catArr));

              if(resp.data.hasOwnProperty('aggregations')){

    		        var jsonForChart = {};
    		        var categories = [];
    		        var arrayForChart = [];
    		        var result = resp.data.aggregations.datagroup.buckets;
    		        var formatDate = function(tgl){
    		          return tgl.getFullYear() + '-' + ('0' + (tgl.getMonth()+1)).slice(-2) + '-' + ('0' + tgl.getDate()).slice(-2);
    		        };
    		        var monthString = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    		        var firstDate = '';
    		        var lastDate = '';
    		        var highestDate = '';
    		        var highestValue = 0;
    		        var highestCategory = '';
                var seriesArr = [];

    		        //for looping xAxis category
    		        for(var i=0;i<result.length;i++){
    		          	var buckets = result[i]
    		          	var categoryBuckets = buckets.category.buckets;

    		        	for(var j=0;j<categoryBuckets.length;j++){
    		            	var dateKey = categoryBuckets[j].key_as_string;
    		            	if(categories.indexOf(dateKey) == -1){
    		            		categories.push(dateKey);
    		            	}
    		        	}
    		        }
    		        categories.sort();

    		        for(var i=0;i<result.length;i++){
    		        	if(catArr.data.raw.indexOf(result[i].key) > -1){

    			            var buckets = result[i];
    			            var categoryBuckets = buckets.category.buckets;
    			            var resDateArr = [];
    			            var tempArray = [];

    			            for(var j=0;j<categoryBuckets.length;j++){
    			            	resDateArr.push({
    			                	date: categoryBuckets[j].key_as_string,
    			                	value: categoryBuckets[j].doc_count
    			            	});
    			            }

    			            for(var j=0;j<categories.length;j++){
    			            	var tempValue = 0;
    			            	for(var k=0;k<resDateArr.length;k++){
    			            		if(resDateArr[k].date === categories[j]){
    			                  		// GET HIGHEST
    			                		if(resDateArr[k].value > highestValue){
    					                    highestCategory = result[i].key;
    					                    highestDate = resDateArr[k].date;
    					                    highestValue = resDateArr[k].value;
    			                		}

    			                		tempValue = resDateArr[k].value;
    			                		break;
    			                	}
    			            	}
    			            	tempArray.push(tempValue);
    			            }

                      seriesArr.push(buckets.key);
                      arrayForChart.push(tempArray);
    		        	}
    		        }

    		        return cb(null, {
                  labels: categories,
                  series: seriesArr,
                  data: arrayForChart,
                  highest: {
                     category: highestCategory,
                     name: highestDate,
                     value: highestValue
                  }
    		        });

              } else{
                return cb('didn\'t get any response', null);
              }

  	    	});
  	    }
	    ], function(err, result){
	      if(err){
	       console.log(err);
         res.status(302);
	       res.json(config.responseMessage(302, false, 'Data not found', err));
        //  res.end();
         return;
          // res.writeHead(302);
          // res.write(config.responseMessage(302, false, 'Data not found', err));
          // return res.end(config.responseMessage(302, false, 'Data not found', err));
	      }

        res.status(200);
        return res.json(config.responseMessage(200, true, 'Getting result from graph4', result));
	  });
	};
  

  this.graph6 = function(req, res, next){
    var token           = jwt.decode(req.headers['x-access-token']) || '';
    var client_id       = token.client_id;
    var keys            = token.keys;
    var uid             = token.usr_uid;
    var group_category  = (typeof req.body.gc != 'undefined') ? req.body.gc : ''; 
    var media_set       = (typeof req.body.ms != 'undefined') ? req.body.ms : ''; 
    var category_id     = (typeof req.body.sc != 'undefined') ? req.body.sc : ''; 
    var media_id        = (typeof req.body.mi != 'undefined') ? req.body.mi.toString() : '';  
    var time_frame      = (typeof req.body.tf != 'undefined') ? parseInt(req.body.tf) : ''; 
    var date_from       = (typeof req.body.df != 'undefined') ? req.body.df : ''; 
    var date_to         = (typeof req.body.dt != 'undefined') ? req.body.dt : '';


    async.waterfall([
      function(cb){
        getToken(function(token){
          cb(null, token);
        });
      },
      function(token, cb){
        getMediaId(client_id, media_set, token, function(medArr){
          getCategorySet(client_id, group_category, true, token, function(catArr){
            if(media_id !== '00'){
              medArr = [media_id];
            }

            if(category_id !== 'All Category'){
              catArr = [category_id];
            }

            cb(null, token, medArr, catArr);
          });
        });
      },
      function(token, medArr, catArr, cb){
        var dateProcessing = dateFormatting(time_frame, date_from, date_to);

        var assets = {
          'category_id': (catArr.data.raw.length > 0) ? catArr.data.raw : [],
          'media_id': (medArr.data.length > 0) ? medArr.data : [],
          'best': 20,
          'time_frame': time_frame,
          'date_from': dateProcessing.date_from,
          'date_to': dateProcessing.date_from
        };

        var abs = {
          'a': '054B',
          'keys': keys,
          'uid': uid,
          'client_id': client_id,
          'data': new Buffer(JSON.stringify(assets)).toString('base64')
        };


        cb(null, token, abs);
      },
      function(token, params, cb){
        request.post({url: "http://mskapi.antara-insight.id/api-new.php", form: params}, function(err, httpResponse, body){
          var resultVar = JSON.parse(JSON.stringify(body));
          
          cb(null, resultVar);
        });
      }
    ], function(err, result){
      if(err){
         console.log(err);
         return res.json(config.responseMessage(302, false, 'Data not found', {}));
        }

        return res.json(config.responseMessage(200, true, 'Getting result from graph6', result));
    });
  };

  this.oldEWS = function(req, res, next){
    var datas           = req.body;
    var token           = jwt.decode(req.headers['x-access-token']) || '';
    var client_id       = token.client_id;
    var usr_keys        = token.keys;
    var usr_uid         = token.usr_uid;
    var group_category  = (typeof req.body.gc != 'undefined') ? req.body.gc : ''; 
    var media_set       = (typeof req.body.ms != 'undefined') ? req.body.ms : ''; 
    var category_id     = (typeof req.body.sc != 'undefined') ? req.body.sc : ''; 
    var media_id        = (typeof req.body.mi != 'undefined') ? req.body.mi.toString() : '';  
    var time_frame      = (typeof req.body.tf != 'undefined') ? parseInt(req.body.tf) : ''; 
    var date_from       = (typeof req.body.df != 'undefined') ? req.body.df.substring(0, 10) : '';  
    var date_to         = (typeof req.body.dt != 'undefined') ? req.body.dt.substring(0, 10) : '';
    var mediaType       = (typeof req.body.media_type != 'undefined') ? req.body.media_type : 'print';
    var reqJSON         = {};


    if(time_frame == 0 && (date_from == '' || date_to == '')){
      res.json(config.responseMessage(400, false, 'Wrong parameter', {}));
    }

    async.waterfall([
        function(cb){
          getToken(function(token){
            cb(null, token);
         });
        },
        function(token, cb){
          getMediaByType(mediaType, function(medias){
            cb(null, token, medias);
          });
        },
        function(token, medias, cb){
          getCategorySet(client_id, group_category, true, token, function(catArr){

              cb(null, token, medias, catArr);
          });
        },
        function(token, medias, catArr, cb){
          var dateProcessing = dateFormatting(time_frame, date_from, date_to);
          var assets = {
            'category_id': (catArr.data.raw.length > 0) ? catArr.data.raw : [],
            'media_id': (medias.length > 0) ? medias : [],
            'best': 20,
            'time_frame': time_frame,
            'date_from': dateProcessing.date_from,
            'date_to': dateProcessing.date_to
          };

          reqJSON.a = '054B';
          reqJSON.keys = usr_keys;
          reqJSON.uid = usr_uid;
          reqJSON.client_id = client_id;
          reqJSON.data = new Buffer(JSON.stringify(assets)).toString('base64');

          request.post({url: "http://mskapi.antara-insight.id/api-new.php", form: reqJSON}, function(err, httpResponse, body){
            var resultVar = JSON.parse(JSON.stringify(body));

            cb(null, dateProcessing, resultVar);
          });

        },
        function(dateProcessing, results, cb){
          results = JSON.parse(results);
          var EWSData = results.data;
          var EWSResult = [];
          var arrDate, JSONResult = [], temporaryJSON, alertArr = [];
          var disposableArray = [], categories = [];
          var potentialCounter = 0, emergingCounter = 0, currentCounter = 0, crisisCounter = 0;
          var sentimentLabels = ["0", "Potential", "Emerging", "Current", "Crisis"];


          if(typeof EWSData === 'undefined'){
            return cb('error calculating early warning system', null);
          }


          var from = (dateProcessing.date_from !== '') ? new Date(dateProcessing.date_from) : '',
              to = (dateProcessing.date_to !== '') ? new Date(dateProcessing.date_to) : '';

          if (from !== '' && to !== '') {
            from = from.getFullYear() + '-' + ('0' + (from.getMonth() + 1)).slice(-2) + '-' + ('0' + from.getDate()).slice(-2);
            to = to.getFullYear() + '-' + ('0' + (to.getMonth() + 1)).slice(-2) + '-' + ('0' + to.getDate()).slice(-2);
          }
 
          var dateFormatting = function(timeFrame, df, dt){
            var currentDate = new Date(),
              dateFlag = new Date(),
              interval = 'day',
              date_from = df,
              date_to = dt,
              tmpArray = [];

            var from = (date_from !== '') ? new Date(date_from) : '',
                to = (date_to !== '') ? new Date(date_to) : '';

            if (from !== '' && to !== '') {
                from = from.getFullYear() + '-' + ('0' + (from.getMonth() + 1)).slice(-2) + '-' + ('0' + from.getDate()).slice(-2);
                to = to.getFullYear() + '-' + ('0' + (to.getMonth() + 1)).slice(-2) + '-' + ('0' + to.getDate()).slice(-2);
            }

            var dateFormatter = function(tgl){
              return tgl.getFullYear() + '-' + ('0' + (tgl.getMonth()+1)).slice(-2) + '-' + ('0' + tgl.getDate()).slice(-2);
            }

              if(timeFrame !== 0){
                switch(timeFrame){
                    case 1:
                      tmpArray.push(dateFormatter(dateFlag));
                      for(var i=0;i<1;i++){
                        dateFlag.setDate(dateFlag.getDate() - 1);
                        tmpArray.push(dateFormatter(dateFlag));
                      }
                      break;
                    case 7:
                      tmpArray.push(dateFormatter(dateFlag));
                      for(var i=0;i<6;i++){
                        dateFlag.setDate(dateFlag.getDate() - 1);
                        tmpArray.push(dateFormatter(dateFlag));
                      }
                      break;
                    case 30:
                      tmpArray.push(dateFormatter(dateFlag));
                      for(var i=0;i<29;i++){
                        dateFlag.setDate(dateFlag.getDate() - 1);
                        tmpArray.push(dateFormatter(dateFlag));
                      }
                      break;
                    case 365:
                      tmpArray.push(dateFormatter(dateFlag));
                      for(var i=0;i<364;i++){
                        dateFlag.setDate(dateFlag.getDate() - 1);
                        tmpArray.push(dateFormatter(dateFlag));
                      }
                      break;
                }
            } else{
              var oneDay = 24*60*60*1000;
              var firstDate = new Date(date_from);
              var secondDate = new Date(date_to);
              var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));

              for(var i=0;i<diffDays;i++){
                dateFlag.setDate(dateFlag.getDate() - 1);
                tmpArray.push(dateFormatter(dateFlag));
              }
            }

            return tmpArray;
          };

          arrDate = dateFormatting(time_frame, from, to).sort();

          for(var i=0;i<EWSData.length;i++){
            var dateDataContainer = [];

            categories.push(EWSData[i].category_id);

            for(var j=0;j<arrDate.length;j++){
              dateDataContainer.push(parseFloat(EWSData[i][arrDate[j]]));


              // issues counter
              if(Math.round(EWSData[i][arrDate[j]]) == 0 || Math.round(EWSData[i][arrDate[j]]) == 1){
                ++potentialCounter;
              } else if(Math.round(EWSData[i][arrDate[j]]) == 2){
                ++emergingCounter;
              } else if(Math.round(EWSData[i][arrDate[j]]) == 3){
                ++currentCounter;
                alertArr.push({
                  category: EWSData[i].category_id,
                  level: 'Current',
                  date: arrDate[j]
                });
              } else if(Math.round(EWSData[i][arrDate[j]]) == 4){
                ++crisisCounter;
                alertArr.push({
                  category: EWSData[i].category_id,
                  level: 'Crisis',
                  date: arrDate[j]
                });
              }
            }

            disposableArray.push(dateDataContainer);
          }


          // sort alert
          alertArr = alertArr.sort(function(a, b) {
            return new Date(b.date) - new Date(a.date);
          });


          var finalResult = {};
          finalResult.labels = arrDate;
          finalResult.series = categories;
          finalResult.data = disposableArray;
          finalResult.gaugeConfig = {
            potential: potentialCounter,
            emerging: emergingCounter,
            current: currentCounter,
            crisis: crisisCounter,
            total: finalResult.labels.length * finalResult.series.length
          };
          finalResult.alertProperties = alertArr;


          // cb(null, JSONResult);
          cb(null, finalResult);
        }
      ], function(err, result){
        if(err){
         console.log(err);
         return res.json(config.responseMessage(302, false, 'Data not found', {}));
        }

        return res.json(config.responseMessage(200, true, 'Getting result from graph4', result));
    });


  };


  this.getNewsList = function(req, res){
    var token           = jwt.decode(req.headers['x-access-token']) || '';
    var client_id       = token.client_id;
    var group_category  = (typeof req.body.gc != 'undefined') ? req.body.gc : ''; 
    var media_set       = (typeof req.body.ms != 'undefined') ? req.body.ms : ''; 
    var category_id     = (typeof req.body.sc != 'undefined') ? req.body.sc : ''; 
    var media_id        = (typeof req.body.mi != 'undefined') ? req.body.mi.toString() : '';  
    var time_frame      = (typeof req.body.tf != 'undefined') ? parseInt(req.body.tf) : ''; 
    var date_from       = (typeof req.body.df != 'undefined') ? req.body.df.substring(0, 10) : '';  
    var date_to         = (typeof req.body.dt != 'undefined') ? req.body.dt.substring(0, 10) : '';
    var mediaType       = (typeof req.body.media_type != 'undefined') ? req.body.media_type : 'print';
    var offset          = (typeof req.body.offset !== 'undefined') ? parseInt(req.body.offset) : 0;
    var article_limit   = (typeof req.body.limit !== 'undefined') ? parseInt(req.body.limit) : 10;

    async.waterfall([
      function(cb){
        getToken(function(digivlaToken){
          cb(null, digivlaToken);
       });
      },
      function(digivlaToken, cb){
        getTVArticle(digivlaToken, client_id, function(tv){
          
          cb(null, digivlaToken, tv);
        });
      },
      function(digivlaToken, tv, cb){
        getMediaByType(mediaType, function(medias){
          cb(null, digivlaToken, medias, tv);
        });
      },
      function(digivlaToken, medias, tv, cb){
        getCategorySet(client_id, group_category, true, digivlaToken, function(catArr){

            if(category_id !== 'All Category'){
                catArr = {
                  success: true,
                  data: {
                    processed: [{
                          match: {
                          category_id: {
                              query: category_id,
                              operator: 'and'
                            }
                        }
                      }],
                      raw: [category_id]
                  }
                };
            }

            cb(null, digivlaToken, medias, catArr, tv);
        });
      },
      function(digivlaToken, medias, catArr, tv, cb){
        var dateProcessing = dateFormatting(time_frame, date_from, date_to);

        var items = {
            category: (catArr.data.processed.length > 0) ? catArr.data.processed : [],
            media: (medias.length > 0) ? medias : [],
            date_from: dateProcessing.date_from,
            date_to: dateProcessing.date_to,
            interval: dateProcessing.interval
        };

        var params = jsonTemplates.getTableList(items);
        cb(null, digivlaToken, medias, catArr, params, tv);
      },
      function(digivlaToken, medias, catArr, params, tv, cb){
        var uriSegment = {
          index: 'client_' + client_id,
          type: 'datalist',
          method: 'POST'
        };

        elasticService(uriSegment, params, function(resp){
          var arrayForTable = [];
          var articleIds = [];
          var buckets, bucketsArray, tempCategory, temp_id;
          var result = resp.data.aggregations.datagroup.buckets;
          var sums = (result.length > 5000) ? 5000 : result.length;
          var flagExists = false;

          for(var i=0;i<sums;i++){
            tempCategory = [];
            temp_id = [];
            buckets = result[i];

            for(var j=0;j<buckets.category.hits.hits.length;j++){
              if(catArr.data.raw.indexOf(buckets.category.hits.hits[j]._source.category_id) > -1){
                tempCategory.push(buckets.category.hits.hits[j]._source.category_id);
                temp_id.push(buckets.category.hits.hits[j]._id);
                flagExists = true;
              }
            }

            if(flagExists){
              articleIds.push(buckets.key);
              arrayForTable.push({
                article_id: buckets.key,
                categories: tempCategory,
                tone: buckets.category.hits.hits[0]._source.tone,
                _id: temp_id
              });
              flagExists = false;
            }
          }

          cb(null, digivlaToken, medias, articleIds, arrayForTable, tv);
        });
      },
      function(digivlaToken, medias, articleIds, arrayForTable, tv, cb){
        var reqJson = {
          "token": digivlaToken,
          "client_id": client_id,
          "article_id": articleIds,
          "limit": arrayForTable.length
        }, uriSegment = {
          first: 'article',
          second: 'search'
        }, 
        ftmi,
        tempResult = [];

        nodeService(uriSegment, reqJson, function(resp2) {
          var articleResult = resp2.data.data;

          for (var i = 0; i < articleResult.length; i++) {
            for (var j = 0; j < arrayForTable.length; j++) {
              if (articleResult[i].article_id.toString() === arrayForTable[j].article_id.toString()) {
                arrayForTable[j].detail = articleResult[i];
                break;
              }
            }
          }


          // please remove this looping below, after fixing the duplicate issues for better performance
          for(var i=0;i<arrayForTable.length;i++){
            ftmi = false;

            if(tempResult.length == 0){
              tempResult.push(arrayForTable[i]);
            } else{

              for(var j=0;j<tempResult.length;j++){
                if( arrayForTable[i].hasOwnProperty('detail') && tempResult[j].hasOwnProperty('detail') ){
                  if(arrayForTable[i].detail.hasOwnProperty('file_pdf') && tempResult[j].detail.hasOwnProperty('file_pdf') ){
                    if((arrayForTable[i].detail.file_pdf === tempResult[j].detail.file_pdf)){
                      ftmi = true;
                    }
                  }
                }
                

              }

              if(!ftmi){
                tempResult.push(arrayForTable[i]);
              }

            }

          }

          cb(null, medias, articleIds, tempResult, tv);
        });
      },
      function(medias, articleIds, resultArr, tv, cb){
        var reqJson = {
          "token": token,
          "client_id": client_id,
          "article_id": articleIds,
          "limit": resultArr.length
        }, uriSegment = {
            first: 'tbl_activity',
            second: ''
        };
        var finalResult = [];

        nodeService(uriSegment, reqJson, function(resp){
          var activityResult = resp.data.data;

            for (var i = 0; i < resultArr.length; i++) {
              if (!resultArr[i].hasOwnProperty('detail')) {
                // resultArr[i].detail = {};

                
              } else{

                // getting file type
                if(resultArr[i].detail.file_pdf.toLowerCase().indexOf('.pdf') > -1){
                  resultArr[i].detail.type = 'pdf';
                } else if(resultArr[i].detail.file_pdf.toLowerCase().indexOf('.mp4') > -1){
                  resultArr[i].detail.type = 'video';
                } else{
                  resultArr[i].detail.type = 'link';
                }
                

                resultArr[i].detail.activity = [];
                if(typeof activityResult === 'object'){
                  for (var j = 0; j < activityResult.length; j++) {
                    if (resultArr[i].article_id.toString() === activityResult[j].article_id.toString() && resultArr[i].detail.activity.indexOf(activityResult[j].activity) == -1) {
                      resultArr[i].detail.activity.push(activityResult[j].activity);
                    }
                  }
                }

                finalResult.push(resultArr[i]);

              }

              
            }

          cb(null, {
            success: true,
            data: finalResult,
            pagination: {
              total_articles: finalResult.length
            },
            tv: tv
          });

        });
      },
    ], function(err, result) {
      if(err){
       console.log(err);
       res.status(302);
       return res.json(config.responseMessage(302, false, 'Data not found', {}));
      }

      res.status(200);
      return res.json(config.responseMessage(200, true, 'Getting result from graph', result));
    });
  };

}















// helper function
/*******************/
function dateFormatting(timeFrame, df, dt){
  var currentDate = new Date(),
    dateFlag = new Date(),
    interval = 'day',
    date_from = df,
    date_to = dt;

  var dateFormatter = function(tgl){
    return tgl.getFullYear() + '-' + ('0' + (tgl.getMonth()+1)).slice(-2) + '-' + ('0' + tgl.getDate()).slice(-2);
  }

    if(timeFrame !== 0){
      switch(timeFrame){
          case 1:
            dateFlag.setDate(dateFlag.getDate() - 1);
            date_from = dateFormatter(dateFlag);
            date_to = dateFormatter(currentDate);
            break;
          case 7:
            dateFlag.setDate(dateFlag.getDate() - 6);
            date_from = dateFormatter(dateFlag);
            date_to = dateFormatter(currentDate);
            break;
          case 30:
            dateFlag.setDate(dateFlag.getDate() - 29);
            date_from = dateFormatter(dateFlag);
            date_to = dateFormatter(currentDate);
            break;
          case 365:
            dateFlag.setDate(dateFlag.getDate() - 364);
            date_from = dateFormatter(dateFlag);
            date_to = dateFormatter(currentDate);
            interval = 'month';
            break;
      }
  } else{
    var oneDay = 24*60*60*1000;
    var firstDate = new Date(date_from);
    var secondDate = new Date(date_to);
    var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));

    if(diffDays > 90){
      interval = 'month';
    }
  }

  return {
    date_from: date_from,
    date_to: date_to,
    interval: interval
  };
}

function getMedia(cb){
  var uriSegment = {
        index: 'media',
        type: 'datalist',
        method: 'POST'
      },
      params = jsonTemplates.getMedia();

    elasticService(uriSegment, params, function(resp){
      if(!resp.success){
          return cb({
            success: false,
            data: []
          });
      }

      var resArr = JSON.parse(JSON.stringify(resp));
      var arrayForChart = {};

      if(resArr.data.hits.hits.length > 0){
      var result = resArr.data.hits.hits;

      for(var i=0;i<result.length;i++){
            arrayForChart[result[i]._source.media_id] = result[i]._source.media_name;
      }

      return cb({
          success: true,
            data: arrayForChart
        });

      } else{
          return cb({
            success: false,
            data: []
          });
      }

    });
};

function getMediaId(client_id, medset, token, cb) {
  var reqJson = {
      client_id: client_id,
      user_media_type_id: parseInt(medset),
      token: token
    },
    uriSegment = {
      first: 'tb_media_set_choosen',
      second: ''
    };

  nodeService(uriSegment, reqJson, function(resp) {
    if (!resp.success) {
      return cb({
        success: false,
        data: []
      });
    }

    var result = [];
    var rawData = resp.data.data;

    for (var i = 0; i < rawData.length; i++) {
      result.push(rawData[i].user_media_id);
    }

    return cb({
      success: true,
      data: result,
      raw: rawData
    });
  });
}

function getMediaDate(tf, df, dt, field){
  var range = {};
  var current = new Date();
  var date_from_ = new Date();

  if(tf == 0 || tf == '0'){
    range[field] = {
      gte: df,
      lte: dt,
      format: "yyyy-MM-dd||yyyy-MM-dd"
    };
  } else{
    var dateFormatter = function(tgl){
      return tgl.getFullYear() + '-' + ('0' + (tgl.getMonth()+1)).slice(-2) + '-' + ('0' + tgl.getDate()).slice(-2);
    };

    switch(tf){
      case 1:
            date_from_.setDate(date_from_.getDate() - 1);
            date_from_ = dateFormatter(date_from_);
            break;
          case 7:
            date_from_.setDate(date_from_.getDate() - 7);
            date_from_ = dateFormatter(date_from_);
            break;
          case 30:
            date_from_.setDate(date_from_.getDate() - 30);
            date_from_ = dateFormatter(date_from_);
            break;
          case 365:
            date_from_.setDate(date_from_.getDate() - 365);
            date_from_ = dateFormatter(date_from_);
            break;
          default:
            date_from_.setDate(date_from_.getDate() - 7);
            date_from_ = dateFormatter(date_from_);
        break;
    }

    range[field] = {
      gte: date_from_,
      lte: dateFormatter(current),
      format: "yyyy-MM-dd||yyyy-MM-dd"
    };
  }

  return range;
}

function getCategorySet(client_id, catset, raw, token, cb) {
  var reqJson = {
      client_id: client_id,
      category_set: parseInt(catset),
      token: token
    },
    uriSegment = {
      first: 'tb_category_set',
      second: ''
    };

  nodeService(uriSegment, reqJson, function(resp) {
    if (!resp.success) {
      return cb({
        success: false,
        data: []
      });
    }

    var rawData = resp.data.data;
    var arrayForChart = [];


    for(var i=0;i<rawData.length;i++){
      var item = {
        "match": {
          "category_id": {
            "query": rawData[i].category_id,
            "operator": "and"
          }
        }
      };
      // var item = {
      //   "term": {
      //     "category_id": rawData[i].category_id
      //   }
      // };

      arrayForChart.push(item);
    }

    if(!raw){
      return cb({
        success: true,
        data: arrayForChart
      });
    } else{
      var rawArr = [];

      for (var i = 0; i < rawData.length; i++) {
        rawArr.push(rawData[i].category_id);
      }

      return cb({
        success: true,
        data: {
          processed: arrayForChart,
          raw: rawArr
        }
      });
    }

  });
}

function mergingPivot(juma, jumb){
  var result = [],
    jumlah;

  for(var i=0;i<juma.length;i++){
    var jumaTgl = juma[i].tgl;
    var jumaCategoryId = juma[i].category_id;
    var jumaJumlah = juma[i].juma;

    for(var j=0;j<jumb.length;j++){
      var jumbTgl = jumb[j].tgl;
      var jumbCategoryId = jumb[j].category_id;
      var jumbJumlah = jumb[j].jumb;

      if(jumaTgl === jumbTgl && jumaCategoryId === jumbCategoryId){
        jumlah = (jumaJumlah === 0) ? 0 : (jumaJumlah / jumbJumlah);

        if(jumaJumlah >= 10){
          if(jumlah >= 0 && jumlah <= 0.5){
            jumlah += 2;
          } else if(jumlah > 0.5 && jumlah <= 0.75){
            jumlah += 3;
          } else{
            jumlah += 4;
          }
        } else{
          jumlah += 1;
        }

        if(jumlah > 4){
          jumlah = 4;
        }

        result.push({
          jumlah: jumlah.toFixed(4),
          category_id: jumaCategoryId,
          tgl: jumaTgl
        });
      }
    }
  }

  return result;
}

function mappingPivotTone(index_pivot, type_pivot, pivot_tone, cb){
  var items = {
    index: index_pivot,
    type: type_pivot
  };
  var params = jsonTemplates.mappingPivotTones(items),
    uriSegment = {
        index: index_pivot,
        type: false,
        method: 'PUT'
    };
// console.log('mappingPivotTones');
// console.log(JSON.stringify(params));
  // check pivot indexing status
  elasticService({index: index_pivot, type: false, method: 'GET'}, {}, function(resp){
    var firstResp = JSON.parse(JSON.stringify(resp));
    if(firstResp.data.hasOwnProperty('status') || (firstResp.data.status == 404 || firstResp.data.status == '404')){

      elasticService(uriSegment, params, function(resp2){
        if(resp2.success){

          /***** BULKING *****/

          // Untuk membatasi hanya kirim 500
          /*var totalBulk = 500;
          var totalindexing = pivot_tone.length;
          var counttt = Math.ceil(totalindexing / totalbulk);
          var lastkey = 0;
          var bulkArr = [];

          for(var i=0;i<=counttt;i++){
            if(pivot_tone.length > 0){
              for(var j=lastkey;j<totalBulk;++j){
                bulkArr.push({
                  index: {
                    _index: index_pivot,
                    _type: type_pivot
                  }
                });
                bulkArr.push(pivot_tone[j]);
                lastkey = j;
                if(j>=(totalindexing-1)){ break; }
              }
            }

            client.bulk({
              body: bulkArr
            }, function(err, resp){
              cb(true);
            });
          }*/

          // Kirim semua
          var bulkArr = [];
          for(var i=0;i<pivot_tone.length;i++){
            bulkArr.push({
              index: {
                _index: index_pivot,
                _type: type_pivot
              }
            });
            bulkArr.push({
              jumlah: parseFloat(pivot_tone[i].jumlah),
              category_id: pivot_tone[i].category_id,
              tgl: pivot_tone[i].tgl
            });
          }

          client.bulk({
            body: bulkArr
          }, function(err, resp){
            return cb(true);
          });

          /***** BULKING *****/

        } else{
          return cb(false);
        }
      });

    }
  });

  return params;
}
/*******************/


function getArticleIds(data){
  if(data.length > 0){
    var arrayForArt = [];
    for(var i=0; i < data.length; i++){
      arrayForArt.push(data[i].article_id); 
    }

    return arrayForArt;
  } else{
    return [];
  }
}

function getToken(callback){
	var auth = new Buffer('digivla' + ':' + 'nzVV2$/(zTH~>m3V').toString('base64');
	  var req = {
	    uri: config.apiAddress + "/token",
	    method: 'POST',
	    headers: {
	        Authorization: 'Basic ' + auth,
	        'Content-Type': 'application/json'
	    },
	    json: {}
	  };

	request(req, function(err, httpResponse, body){
		if(err){
			return callback({
				success: false,
				data: err
			});
		}

		var resultVar = JSON.parse(JSON.stringify(body));

		return callback(resultVar.data);
	});
}

function getMediaByType(mediaType, cb){
  switch(mediaType){
    case 'print':
      mediaTypeArr = [1, 2, 3, 6, 7, 9, 11];
      break;
    case 'online':
      mediaTypeArr = [4, 5];
      break;
    case 'tv':
      mediaTypeArr = [12];
      break;
    case 'radio':
      mediaTypeArr = [13];
      break;
    case 'all':
      mediaTypeArr = [];
      break;
    default:
      mediaTypeArr = [];
      break;
  }

  async.waterfall([

    function(cb){
      getToken(function(token){
        cb(null, token);
      });
    },
    function(token, cb){
      var items = {
        'token': token,
        'statuse': ['A'],
        'media_type_id': mediaTypeArr
      }, uriSegment = {
        first: 'tb_media',
        second: ''
      };

      nodeService(uriSegment, items, function(resp) {
        if(!resp.success){
          cb('error getting media', []);
        } else{
          var result = resp.data.data;
          var medIdArr = [];

          for(var i=0;i<result.length;i++){
            medIdArr.push(result[i].media_id);
          }

          cb(null, medIdArr);
        }

      });
    }
  ], function(err, result){
    if(err){
      console.log(err);
      return cb(false);
    }

    return cb(result);
  });
}

function getTVArticle(digivlaToken, client_id, cb){
  var sizes = 100;
  async.waterfall([
    function(cb){
      getMediaByType('tv', function(medias){
        cb(null, medias);
      });
    },
    function(medias, cb){
      var items = {
        size: sizes,
        offset: 0,
        media: (medias.length > 0) ? medias : [],
      };

      var params = jsonTemplates.getTVArticle(items);
      cb(null, params);
    },
    function(params, cb){
      var uriSegment = {
        index: 'client_' + client_id,
        type: 'datalist',
        method: 'POST'
      };

      elasticService(uriSegment, params, function(resp){
        var article_id = [];

        for(var i=0;i<resp.data.hits.hits.length;i++){
          article_id = [resp.data.hits.hits[i]._source.article_id];
        }

        cb(null, article_id);
      });
    },
    function(article_id, cb){
      var reqJson = {
        "token": digivlaToken,
        "client_id": client_id,
        "article_id": article_id,
        "limit": sizes
      }, uriSegment = {
        first: 'article',
        second: 'search'
      }

      nodeService(uriSegment, reqJson, function(resp) {
        var articleResult = resp.data.data;
        var finalResult;

        for(var i=0;i<articleResult.length;i++){
          if(articleResult[i].hasOwnProperty('file_pdf')){
            finalResult = articleResult[i];
            break;
          }

        }

        return cb(null, finalResult);

      });
    }
  ], function(err, result){
    if(err) return cb(false);
    return cb(result);
  });
}

module.exports = new formedController();
