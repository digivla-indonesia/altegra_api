var mysql			= require('mysql');
var path			= require('path');
var config          = require(path.resolve('./app/config/config'));
var request			= require('request');
var jwt             = require('jsonwebtoken');
var async           = require('async');
var is_json 	    = require('is-json');


function socmedController(){

  this.getWordCloud = function(req, res, next){
  	var token           = jwt.decode(req.headers['x-access-token']) || '';
    var client_id       = req.body.client_id;
    var dateRange		= dateFormatter(30, '', '');

    var options			= {
    	query: 'SELECT word, SUM(value) AS mentioned FROM '+ client_id +' WHERE datee BETWEEN "'+ dateRange.date_from +'" AND "'+ dateRange.date_to +'" GROUP BY word ORDER BY mentioned DESC LIMIT 250'
    }

    var connection = mysql.createConnection({
		host     : config.mysqlDBHost,
		user     : config.alMysqlUsername,
		password : config.alMysqlPassword,
		database : config.mysqlDBWordCloud
	});

    connection.connect();
 
	connection.query(options.query, function (error, results, fields) {
		connection.end();
		if (error){
			res.status(400);
      		return res.json(config.responseMessage(400, false, 'Cannot get results from word cloud', []));
		}


		if(results.length > 0){
			var highestValue = results[0].mentioned, lowestValue = results[0].mentioned, bucketsArr = [];

			for(var i=0;i<results.length;i++){
				if(lowestValue > results[i].mentioned){
					lowestValue = results[i].mentioned;
				}

				if(highestValue < results[i].mentioned){
					highestValue = results[i].mentioned;
				}

				bucketsArr.push([results[i].word, results[i].mentioned]);
			}


			res.status(200);
	      	return res.json(config.responseMessage(200, true, 'Getting result for word cloud', {min: lowestValue, max: highestValue, words: bucketsArr}));
		} else{
			res.status(400);
      		return res.json(config.responseMessage(400, false, 'No data', results));
		}

	});
	 
  };

};


module.exports = new socmedController();


// helper function
function dateFormatter(timeFrame, df, dt){
	var currentDate = new Date(),
    dateFlag = new Date(),
    interval = 'day',
    date_from = df,
    date_to = dt;

  var dateFormatter = function(tgl) {
    return tgl.getFullYear() + '-' + ('0' + (tgl.getMonth() + 1)).slice(-2) + '-' + ('0' + tgl.getDate()).slice(-2);
  }

  if (timeFrame !== 0) {
    switch (timeFrame) {
      case 1:
        dateFlag.setDate(dateFlag.getDate() - 1);
        date_from = dateFormatter(dateFlag);
        date_to = dateFormatter(currentDate);
        break;
      case 7:
        dateFlag.setDate(dateFlag.getDate() - 6);
        date_from = dateFormatter(dateFlag);
        date_to = dateFormatter(currentDate);
        break;
      case 30:
        dateFlag.setDate(dateFlag.getDate() - 29);
        date_from = dateFormatter(dateFlag);
        date_to = dateFormatter(currentDate);
        break;
      case 365:
        dateFlag.setDate(dateFlag.getDate() - 364);
        date_from = dateFormatter(dateFlag);
        date_to = dateFormatter(currentDate);
        interval = 'month';
        break;
    }
  } else {
    if (date_to == '' || date_from == '') {
      dateFlag.setDate(dateFlag.getDate() - 7);
      date_from = dateFormatter(dateFlag);
      date_to = dateFormatter(currentDate);
    }
  }

  return {
    date_from: date_from,
    date_to: date_to,
    interval: interval
  };
}
