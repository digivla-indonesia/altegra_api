var jwt = require('jsonwebtoken');
var config = require('../config/config');
var mysql = require('mysql');
var jwt = require('jsonwebtoken');

var pool  = mysql.createPool({
  connectionLimit : 2,
  host            : config.mysqlSlaveDBHost,
  user            : config.mysqlDBUsername,
  password        : config.mysqlDBPassword,
  database        : config.mysqlSlaveDBName
});

var auth = {
	login: function(req, res) {
		var username = req.body.username || '';
		var password = req.body.password || '';

		if (username == '' || password == '') {
			res.status(401);
			return res.json(config.responseMessage(401, false, 'Harap lengkapi username atau password', ''));
		}

		auth.getUser(username, password, function(err, results){
			if(err){
				res.status(401);
				return res.json(config.responseMessage(401, false, 'Username atau password tidak sesuai', ''));
			}

			if(results.length > 0){
				var token = jwt.sign(results[0], config.jwtsecret, {
					// expiresIn: "1y"
					expires: expiresIn(365)
					//expiresIn: 60
				});
				res.status(200);
				return res.json(config.responseMessage(200, true, 'Logged in successfully', token));
			} else{
				res.status(401);
				return res.json(config.responseMessage(401, false, 'Username atau password tidak sesuai', ''));
			}

		});
	},

	getUser: function(username, password, callback) {
		var dateFormatter = function(tgl){
			return tgl.getFullYear() + '-' + ('0' + (tgl.getMonth()+1)).slice(-2) + '-' + ('0' + tgl.getDate()).slice(-2);
		};

		pool.getConnection(function(err, connection) {
			var options = {sql: "SELECT * FROM (`tbl_user_comp` as tuc) JOIN `tbl_company` as tc ON `tuc`.`comp_id`=`tc`.`comp_id` JOIN `tbl_agent` as ta ON `tc`.`agent_id`=`ta`.`agent_id` WHERE `tuc`.`usr_comp_login` = '"+username+"' AND `tuc`.`usr_comp_passwd` = '"+password+"' AND `tc`.`comp_status` IN ('ACTIVE','TRIAL') AND `tc`.`comp_expired` >= '"+dateFormatter(new Date())+"'"};
			// Use the connection
			connection.query(options.sql, function (error, results, fields) {
				// And done with the connection.
				connection.release();
				
				callback(null, results);
				if (error) return callback(error, null);

				// Don't use the connection here, it has been returned to the pool.
			});
		});
	}
};

/** private methods **/

// generate token
function genToken(user) {
	var expires = expiresIn(7); // 7 days
	var token = jwt.encode({
		exp: expires
	}, config.jwtsecret);

	return {
		token: token,
		expires: expires,
		user: user.username
	};
}

function expiresIn(numDays) {
	var dateObj = new Date();
	return dateObj.setDate(dateObj.getDate() + numDays);
}

module.exports = auth;
